package com.mcguiness.loan;

import com.mcguiness.loan.controller.ApplicationController;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main.java - Main Driver for the application.
 *
 * @author Andrew McGuiness
 * @version 29/Mar/2017
 */
public class Main extends Application {
    public static void main( String[] args ) throws Exception {
        launch( args );
    }

    @Override
    public void start( Stage primaryStage ) throws Exception {
        new ApplicationController( primaryStage );
    }
}
